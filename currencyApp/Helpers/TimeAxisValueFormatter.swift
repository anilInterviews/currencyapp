//
//  TimeAxisValueFormatter.swift
//  currencyApp
//
//  Created by Anıl Aydın on 8.10.2023.
//

import Foundation
import DGCharts

class TimeAxisValueFormatter: IndexAxisValueFormatter {
    
    override func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let months = ["Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        
        switch value {
        case 0:
            return "\(months[0]) '20"
        case 1...12:
            return "\(months[Int(value)]) '21"
        case 13...24:
            return "\(months[Int(value) - 12]) '22"
        case 25:
            return "\(months[0]) '23"
        default:
            return ""
        }
    }
    
}

//
//  SVGImageProcessor.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit
import Kingfisher
import SVGKit

struct SVGImgProcessor: ImageProcessor {
    let identifier: String = "com.currencyApp.svgProcessor"
    
    func process(item: ImageProcessItem, options: KingfisherParsedOptionsInfo) -> KFCrossPlatformImage? {
        switch item {
        case .image(let image):
            return image
        case .data(let data):
            let imageSvg = SVGKImage(data: data)
            return imageSvg?.uiImage
        }
    }
}

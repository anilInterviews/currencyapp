//
//  AppDelegate.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: CurrencyListViewController())
        window?.makeKeyAndVisible()
        return true
    }

}

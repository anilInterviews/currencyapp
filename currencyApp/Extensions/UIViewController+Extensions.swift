//
//  UIViewController+Extensions.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit

extension UIViewController {
    
    func displayErrorAlert(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

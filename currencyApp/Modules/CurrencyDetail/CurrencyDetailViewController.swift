//
//  CurrencyDetailViewController.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit

final class CurrencyDetailViewController: UIViewController {

    private lazy var viewModel = CurrencyDetailViewModel(coin: coin)
    private var detailView = CurrencyDetailView()
    private var coin: Coin
    
    private var isBellActive = false
    
    private lazy var bellButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "bell"), for: .normal)
        button.addTarget(self, action: #selector(bellTapped), for: .touchUpInside)
        return button
    }()
    
    init(coin: Coin) {
        self.coin = coin
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        bindViewModel()
    }
    
    override func loadView() {
        view = detailView
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.backIndicatorImage = UIImage(systemName: "chevron.left")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(systemName: "chevron.left")
        navigationController?.navigationBar.tintColor = UIColor("#1B2759")
        
        let titleView = UIStackView()
        titleView.axis = .vertical
        titleView.alignment = .center
        
        let symbolLabel = UILabel()
        symbolLabel.text = viewModel.coinSymbol
        symbolLabel.textColor = UIColor("#B0B5C9")
        symbolLabel.font = .systemFont(ofSize: 15, weight: .regular)
        titleView.addArrangedSubview(symbolLabel)
        
        let nameLabel = UILabel()
        nameLabel.text = viewModel.coinName
        nameLabel.textColor = UIColor("#1B2759")
        nameLabel.font = .systemFont(ofSize: 16, weight: .heavy)
        titleView.addArrangedSubview(nameLabel)
        
        navigationItem.titleView = titleView
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: bellButton)
    }
    
    private func bindViewModel() {
        detailView.set(price: viewModel.currentPrice)
        detailView.set(change: viewModel.changeValue)
        detailView.set(changeColor: viewModel.changeColor)
        detailView.set(high: viewModel.highValue)
        detailView.set(low: viewModel.lowValue)
        
        let openValues = viewModel.graphValues.map { $0.open }
        let closeValues = viewModel.graphValues.map { $0.close }
        let highValues = viewModel.graphValues.map { $0.high }
        let lowValues = viewModel.graphValues.map { $0.low }
        detailView.setCandleChartData(open: openValues, close: closeValues, high: highValues, low: lowValues)
    }
    
    private func updateBellButton() {
        let bellImageName = isBellActive ? "bell.fill" : "bell"
        bellButton.setImage(UIImage(systemName: bellImageName), for: .normal)
    }
    
    @objc private func bellTapped() {
        isBellActive.toggle()
        updateBellButton()
    }
    
}

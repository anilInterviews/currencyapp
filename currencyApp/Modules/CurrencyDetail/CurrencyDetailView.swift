//
//  CurrencyDetailView.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit
import SnapKit
import DGCharts

class CurrencyDetailView: UIView {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor("#1B2759")
        label.text = "CURRENT PRICE"
        label.font = .systemFont(ofSize: 13, weight: .light)
        label.textAlignment = .left
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 22, weight: .heavy)
        label.textColor = UIColor("#1B2759")
        label.textAlignment = .left
        return label
    }()
    
    private lazy var changeLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 13, weight: .heavy)
        label.textAlignment = .left
        return label
    }()
    
    private lazy var highLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = .systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    private lazy var lowLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.font = .systemFont(ofSize: 16, weight: .regular)
        return label
    }()
    
    private lazy var candleStickChartView: CandleStickChartView = {
        let chartView = CandleStickChartView()
        return chartView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = .white
        
        addSubview(titleLabel)
        addSubview(priceLabel)
        addSubview(changeLabel)
        addSubview(highLabel)
        addSubview(lowLabel)
        addSubview(candleStickChartView)
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(32)
            make.top.equalTo(safeAreaLayoutGuide).offset(32)
        }
        
        priceLabel.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
        }
        
        changeLabel.snp.makeConstraints { make in
            make.leading.equalTo(priceLabel)
            make.top.equalTo(priceLabel.snp.bottom).offset(8)
        }
        
        highLabel.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-32)
            make.top.equalTo(priceLabel)
        }
        
        lowLabel.snp.makeConstraints { make in
            make.trailing.equalTo(highLabel)
            make.top.equalTo(highLabel.snp.bottom).offset(8)
        }
        
        candleStickChartView.snp.makeConstraints { make in
            make.top.equalTo(lowLabel.snp.bottom).offset(64)
            make.leading.trailing.equalToSuperview().inset(32)
            make.height.equalTo(200)
        }
    }
    
}

// MARK: - Public Functions
extension CurrencyDetailView {
    
    func set(price: String) {
        priceLabel.text = price
    }
    
    func set(change: String) {
        changeLabel.text = change
    }
    
    func set(high: String) {
        let highLabelString = "High: \(high)"
        let attributedString = NSMutableAttributedString(string: highLabelString)

        if let highRange = highLabelString.range(of: "High:") {
            attributedString.addAttribute(.foregroundColor, value: UIColor("#1B2759"), range: NSRange(highRange, in: highLabelString))
        }
        if let valueRange = highLabelString.range(of: high) {
            attributedString.addAttribute(.foregroundColor, value: UIColor.green, range: NSRange(valueRange, in: highLabelString))
        }

        highLabel.attributedText = attributedString
    }
    
    func set(low: String) {
        let lowLabelString = "Low: \(low)"
        let attributedString = NSMutableAttributedString(string: lowLabelString)

        if let lowRange = lowLabelString.range(of: "Low:") {
            attributedString.addAttribute(.foregroundColor, value: UIColor("#1B2759"), range: NSRange(lowRange, in: lowLabelString))
        }
        if let valueRange = lowLabelString.range(of: low) {
            attributedString.addAttribute(.foregroundColor, value: UIColor.red, range: NSRange(valueRange, in: lowLabelString))
        }

        lowLabel.attributedText = attributedString
    }
    
    func set(changeColor: UIColor) {
        changeLabel.textColor = changeColor
    }
    
    func setCandleChartData(open: [Double], close: [Double], high: [Double], low: [Double]) {
        var dataEntries: [CandleChartDataEntry] = []
        
        for i in 0..<open.count {
            let dataEntry = CandleChartDataEntry(x: Double(i), shadowH: high[i], shadowL: low[i], open: open[i], close: close[i])
            dataEntries.append(dataEntry)
        }
        
        let dataSet = CandleChartDataSet(entries: dataEntries, label: "Price chart")
        dataSet.colors = [NSUIColor.green, NSUIColor.red]
        dataSet.decreasingColor = UIColor.red
        dataSet.increasingColor = UIColor.green
        dataSet.increasingFilled = true
        dataSet.shadowColorSameAsCandle = true
        dataSet.valueTextColor = .clear
        
        let chartData = CandleChartData(dataSet: dataSet)
        candleStickChartView.data = chartData
        
        let xAxis = candleStickChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.valueFormatter = TimeAxisValueFormatter()
    }
    
}

//
//  CurrencyDetailViewModel.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit

class CurrencyDetailViewModel {
    
    private let coin: Coin
    
    init(coin: Coin) {
        self.coin = coin
    }
    
    var coinSymbol: String {
        return coin.symbol
    }
    
    var coinName: String {
        return coin.name
    }
    
    var currentPrice: String {
        let price: Double = Double(coin.price) ?? 0.0
        return String(format: "$%.2f", price)
    }
    
    var changeValue: String {
        let percentageChange: Double = Double(coin.change) ?? 0.0
        let price: Double = Double(coin.price) ?? 0.0
        let changeValue = (price * percentageChange) / 100.0
        
        if changeValue < 0 {
            return String(format: "%+.2f%% (-$%.2f)", percentageChange, abs(changeValue))
        } else {
            return String(format: "%+.2f%% (+$%.2f)", percentageChange, changeValue)
        }
    }
    
    var changeColor: UIColor {
        let changeValue = Double(coin.change) ?? 0.0
        return (changeValue < 0) ? .red : .green
    }
    
    var highValue: String {
        let maxValue = coin.sparkline.compactMap { Double($0) }.max() ?? 0
        return String(format: "%.2f", maxValue)
    }
    
    var lowValue: String {
        let minValue = coin.sparkline.compactMap { Double($0) }.min() ?? 0
        return String(format: "%.2f", minValue)
    }
    
    var graphValues: [(open: Double, high: Double, low: Double, close: Double)] {
        var ohlc: [(open: Double, high: Double, low: Double, close: Double)] = []
        
        let sparklineValues = coin.sparkline.compactMap { Double($0) }
        
        for index in 0..<sparklineValues.count {
            let close = sparklineValues[index]
            let open = (index == 0) ? close : sparklineValues[index - 1]
            let high = max(open, close)
            let low = min(open, close)
            ohlc.append((open: open, high: high, low: low, close: close))
        }
        
        return ohlc
    }
    
}

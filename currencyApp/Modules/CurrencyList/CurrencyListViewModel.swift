//
//  CurrencyListViewModel.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

enum SortingOption: String, CaseIterable {
    case price, marketCap, volume24h, change, listedAt
    
    var displayName: String {
        switch self {
        case .price: return "Price"
        case .marketCap: return "Market Cap"
        case .volume24h: return "24h Volume"
        case .change: return "Change"
        case .listedAt: return "Listed At"
        }
    }
}

class CurrencyListViewModel {
    
    private var networkService: NetworkServiceProtocol
    var coins: [Coin] = []
    
    init(networkService: NetworkServiceProtocol = NetworkService()) {
        self.networkService = networkService
    }
    
    var currencies: [CurrencyViewModel] {
        return coins.map { CurrencyViewModel(currency: $0) }
    }
    
    var sortingOptions: [SortingOption] {
        return SortingOption.allCases
    }
    
    func sortCurrencies(by option: SortingOption) {
        switch option {
        case .price:
            coins.sort(by: { Double($0.price) ?? 0 > Double($1.price) ?? 0 })
        case .marketCap:
            coins.sort(by: { Double($0.marketCap) ?? 0 > Double($1.marketCap) ?? 0 })
        case .volume24h:
            coins.sort(by: { Double($0.the24HVolume) ?? 0 > Double($1.the24HVolume) ?? 0 })
        case .change:
            coins.sort(by: { Double($0.change) ?? 0 > Double($1.change) ?? 0 })
        case .listedAt:
            coins.sort(by: { $0.listedAt < $1.listedAt })
        }
    }

    func fetchCoins(completion: @escaping (Error?) -> Void) {
        let request = CoinsAPIRequest()
        networkService.execute(request) { (result: Result<CoinsResponse, Error>) in
            switch result {
            case .success(let response):
                self.coins = response.data.coins
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    func errorMessage(for error: Error) -> String {
        if let networkError = error as? NetworkError {
            return networkError.description
        } else {
            return "Something went wrong. Please try again."
        }
    }
    
}

//
//  CurrencyListViewController.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit
import SnapKit
import Kingfisher

final class CurrencyListViewController: UIViewController {

    private var viewModel = CurrencyListViewModel()
    private var currencyListView = CurrencyListView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCurrencyListView()
        fetchCoins()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func loadView() {
        super.loadView()
        
        view = currencyListView
    }
    
    private func setupCurrencyListView() {
        currencyListView.delegate = self
        currencyListView.tableView.dataSource = self
        currencyListView.tableView.delegate = self
        currencyListView.tableView.register(CurrencyTableViewCell.self, forCellReuseIdentifier: "CurrencyCell")
        currencyListView.sortingOptions = viewModel.sortingOptions
    }
    
    private func fetchCoins() {
        viewModel.fetchCoins { error in
            if let error = error {
                let message = self.viewModel.errorMessage(for: error)
                self.displayErrorAlert(message: message)
            } else {
                DispatchQueue.main.async {
                    self.currencyListView.tableView.reloadData()
                }
            }
        }
    }
    
}

// MARK: - UITableViewDelegate
extension CurrencyListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCoin = viewModel.coins[indexPath.row]
        let detailVC = CurrencyDetailViewController(coin: selectedCoin)
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
}

// MARK: - UITableViewDataSource
extension CurrencyListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.currencies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell", for: indexPath) as! CurrencyTableViewCell
        let currencyViewModel = viewModel.currencies[indexPath.row]
        cell.configure(with: currencyViewModel)
        return cell
    }
    
}

// MARK: - CurrencyListViewDelegate
extension CurrencyListViewController: CurrencyListViewDelegate {
    
    func didSelectFilterOption(_ option: SortingOption) {
        viewModel.sortCurrencies(by: option)
        currencyListView.tableView.reloadData()
        currencyListView.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
}

//
//  CurrencyListView.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit
import SnapKit

protocol CurrencyListViewDelegate: AnyObject {
    func didSelectFilterOption(_ option: SortingOption)
}

class CurrencyListView: UIView {
    
    weak var delegate: CurrencyListViewDelegate?
    
    var sortingOptions: [SortingOption] = [] {
        didSet {
            filterButton.setTitle(sortingOptions.first?.displayName, for: .normal)
            pickerView.reloadAllComponents()
        }
    }

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 24, weight: .heavy)
        label.text = "Ranking List"
        label.textColor = UIColor("#1B2759")
        return label
    }()
    
    private lazy var filterButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(UIColor("#7365F3"), for: .normal)
        button.backgroundColor = UIColor("#D0CCE8")
        button.layer.cornerRadius = 16
        button.addTarget(self, action: #selector(filterButtonTapped), for: .touchUpInside)
        return button
    }()
    
    private lazy var pickerTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Filter"
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 21, weight: .heavy)
        label.textColor = UIColor("#1B2759")
        return label
    }()
    
    private lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor("#D0CCE8")
        return picker
    }()
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.backgroundColor = UIColor("#F0F0F0")
        tv.separatorStyle = .none
        return tv
    }()
    
    private var isPickerHidden: Bool = true {
        didSet {
            pickerTitleLabel.isHidden = isPickerHidden
            pickerView.isHidden = isPickerHidden
            configureTableView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        backgroundColor = UIColor("#F0F0F0")
        
        addSubview(titleLabel)
        addSubview(filterButton)
        addSubview(tableView)
        addSubview(pickerView)
        pickerView.addSubview(pickerTitleLabel)
        
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide).offset(16)
            make.leading.equalToSuperview().offset(16)
        }
        
        filterButton.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.centerY.equalTo(titleLabel)
            make.height.equalTo(40)
            make.width.equalTo(120)
        }
        
        tableView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(32)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalTo(safeAreaLayoutGuide)
        }
        
        pickerView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(200)
        }
        
        pickerTitleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16)
            make.leading.trailing.equalToSuperview()
        }
        
        isPickerHidden = true
    }
    
    private func configureTableView() {
        tableView.snp.remakeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(32)
            make.leading.trailing.equalToSuperview().inset(16)
            if isPickerHidden {
                make.bottom.equalTo(safeAreaLayoutGuide)
            } else {
                make.bottom.equalTo(pickerView.snp.top)
            }
        }
        layoutIfNeeded()
    }
    
    @objc private func filterButtonTapped() {
        isPickerHidden.toggle()
    }
}

extension CurrencyListView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortingOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        label.textColor = UIColor("#7365F3")
        label.font = .systemFont(ofSize: 18, weight: .medium)
        label.textAlignment = .center
        label.text = sortingOptions[row].displayName
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedOption = sortingOptions[row]
        filterButton.setTitle(selectedOption.displayName, for: .normal)
        delegate?.didSelectFilterOption(selectedOption)
    }
    
}

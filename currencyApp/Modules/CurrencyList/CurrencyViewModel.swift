//
//  CurrencyViewModel.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import UIKit

class CurrencyViewModel {
    
    let currency: Coin

    init(currency: Coin) {
        self.currency = currency
    }

    var iconURL: URL? {
        return URL(string: currency.iconUrl)
    }

    var symbol: String {
        return currency.symbol
    }

    var name: String {
        return currency.name
    }

    var displayPrice: String {
        let price: Double = Double(currency.price) ?? 0.0
        return String(format: "$%.2f", price)
    }

    var changePercentage: String {
        let percentageChange: Double = Double(currency.change) ?? 0.0
        let price: Double = Double(currency.price) ?? 0.0
        let changeValue = (price * percentageChange) / 100.0
        
        if changeValue < 0 {
            return String(format: "%+.2f%% (-$%.2f)", percentageChange, abs(changeValue))
        } else {
            return String(format: "%+.2f%% (+$%.2f)", percentageChange, changeValue)
        }
    }
    
    var changeColor: UIColor {
        let changeValue = Double(currency.change) ?? 0.0
        return (changeValue < 0) ? .red : .green
    }
    
}

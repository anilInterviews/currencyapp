//
//  CoinResponse.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

struct CoinsResponse: Codable {
    let status: String
    let data: CoinData
}

struct CoinData: Codable {
    let stats: Stats
    let coins: [Coin]
}

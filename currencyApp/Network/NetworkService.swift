//
//  NetworkService.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

struct APIConfig {
    static let baseURL = URL(string: "https://psp-merchantpanel-service-sandbox.ozanodeme.com.tr")!
}

class NetworkService: NetworkServiceProtocol {
    
    func execute<T: Decodable>(_ request: Request, completion: @escaping (Result<T, Error>) -> Void) {
        let url = APIConfig.baseURL.appendingPathComponent(request.path)
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.allHTTPHeaderFields = request.headers
        
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            if error != nil {
                completion(.failure(NetworkError.networkRequestFailed))
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse, (200..<300).contains(httpResponse.statusCode) == false {
                completion(.failure(NetworkError.serverError(statusCode: httpResponse.statusCode)))
                return
            }
            
            if let data = data {
                do {
                    let decodedObject = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(decodedObject))
                } catch {
                    completion(.failure(NetworkError.decodingError))
                }
            } else {
                completion(.failure(NetworkError.networkRequestFailed))
            }
        }.resume()
    }
    
}

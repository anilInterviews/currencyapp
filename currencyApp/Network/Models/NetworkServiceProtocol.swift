//
//  NetworkServiceProtocol.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

protocol NetworkServiceProtocol {
    func execute<T: Decodable>(_ request: Request, completion: @escaping (Result<T, Error>) -> Void)
}

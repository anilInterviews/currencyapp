//
//  Stats.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

struct Stats: Codable {
    let total: Int
    let totalCoins: Int
    let totalMarkets: Int
    let totalExchanges: Int
    let totalMarketCap: String
    let total24hVolume: String
}

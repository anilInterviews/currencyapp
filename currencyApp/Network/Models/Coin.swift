//
//  Coin.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

struct Coin: Codable {
    let uuid: String
    let symbol: String
    let name: String
    let color: String?
    let iconUrl: String
    let marketCap: String
    let price: String
    let listedAt: Int
    let tier: Int
    let change: String
    let rank: Int
    let sparkline: [String]
    let lowVolume: Bool
    let coinrankingUrl: String
    let the24HVolume: String
    let btcPrice: String
    
    enum CodingKeys: String, CodingKey {
        case uuid, symbol, name, color, iconUrl, coinrankingUrl, btcPrice
        case marketCap, price, listedAt, tier, change, rank, sparkline, lowVolume
        case the24HVolume = "24hVolume"
    }
}

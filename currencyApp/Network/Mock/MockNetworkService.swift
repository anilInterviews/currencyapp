//
//  MockNetworkService.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

class MockNetworkService: NetworkServiceProtocol{
    var mockResult: Result<CoinsResponse, Error>?
    
    func execute<T: Decodable>(_ request: Request, completion: @escaping (Result<T, Error>) -> Void) {
        guard let result = mockResult as? Result<T, Error> else {
            fatalError("Couldn't cast mockResult to expected type. Make sure mockResult is set properly in your test.")
        }
        completion(result)
    }
}

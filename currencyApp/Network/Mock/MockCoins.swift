//
//  MockCoins.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

struct MockCoins {
    static let coin1 = Coin(uuid: "1", symbol: "BTC", name: "Bitcoin", color: nil, iconUrl: "", marketCap: "800000000", price: "30000", listedAt: 1624147200, tier: 1, change: "5", rank: 1, sparkline: [""], lowVolume: false, coinrankingUrl: "", the24HVolume: "1000000", btcPrice: "1")
    static let coin2 = Coin(uuid: "2", symbol: "ETH", name: "Ethereum", color: nil, iconUrl: "", marketCap: "600000000", price: "20000", listedAt: 1624148000, tier: 2, change: "3", rank: 2, sparkline: [""], lowVolume: false, coinrankingUrl: "", the24HVolume: "900000", btcPrice: "2")
    static let coin3 = Coin(uuid: "3", symbol: "BNB", name: "Binance Coin", color: "#f7931A", iconUrl: "https://test.com", marketCap: "1000", price: "5000", listedAt: 12345678, tier: 1, change: "10", rank: 1, sparkline: ["4000", "4500", "5000", "5500"], lowVolume: false, coinrankingUrl: "https://test.com", the24HVolume: "20000", btcPrice: "3")

    static let coinsResponse: CoinsResponse = {
        return CoinsResponse(
            status: "success",
            data: CoinData(stats: Stats(total: 1, totalCoins: 1, totalMarkets: 1, totalExchanges: 1, totalMarketCap: "1", total24hVolume: "1"), coins: [coin1, coin2, coin3])
        )
    }()
}

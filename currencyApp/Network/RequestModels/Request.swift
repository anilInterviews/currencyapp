//
//  Request.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

protocol Request {
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: [String: String]? { get }
}

//
//  CoinsApiRequest.swift
//  currencyApp
//
//  Created by Anıl Aydın on 7.10.2023.
//

import Foundation

struct CoinsAPIRequest: Request {
    var path: String {
        return "/api/v1/dummy/coins"
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
}

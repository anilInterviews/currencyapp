//
//  currencyAppTests.swift
//  currencyAppTests
//
//  Created by Anıl Aydın on 7.10.2023.
//

import XCTest
@testable import currencyApp

final class currencyAppTests: XCTestCase {
    
    var currencyViewModel: CurrencyViewModel!
    var viewModel: CurrencyListViewModel!
    var detailViewModel: CurrencyDetailViewModel!
    var mockService: MockNetworkService!
    
    override func setUpWithError() throws {
        mockService = MockNetworkService()
        viewModel = CurrencyListViewModel(networkService: mockService)
        detailViewModel = CurrencyDetailViewModel(coin: MockCoins.coin3)
    }
    
    override func tearDownWithError() throws {
        viewModel = nil
        mockService = nil
    }
    
    // MARK: - CurrencyList Module Tests
    func testFetchCoinsSuccess() {
        let expectation = self.expectation(description: "Fetching coins should succeed")

        mockService.mockResult = .success(MockCoins.coinsResponse)
        viewModel.fetchCoins { error in
            if error != nil {
                XCTFail("Fetching coins should succeed but failed")
            } else {
                expectation.fulfill()
            }
        }

        waitForExpectations(timeout: 5) { (error) in
            if let error = error {
                XCTFail("waitForExpectations failed: \(error)")
            }
        }
    }
    
    func testSortByPrice() {
        mockService.mockResult = .success(MockCoins.coinsResponse)
        viewModel.fetchCoins { _ in }
        viewModel.sortCurrencies(by: .price)
        
        XCTAssertEqual(viewModel.coins[0].symbol, "BTC")
    }

    func testChangePercentage() {
        mockService.mockResult = .success(MockCoins.coinsResponse)
        viewModel.fetchCoins { _ in }
        currencyViewModel = CurrencyViewModel(currency: viewModel.coins[0])
        
        let percentage = currencyViewModel.changePercentage
        XCTAssertEqual(percentage, "+5.00% (+$1500.00)")
    }
    
    // MARK: - CurrencyDetail Module Tests
    func testGraphValuesOpenCloseOrder() {
        let values = detailViewModel.graphValues
        for value in values {
            XCTAssertTrue(value.open <= value.close, "Open should always be less than or equal to close in our test data")
        }
    }
    
    func testGraphValuesConsistencyWithSparkline() {
        let sparklineCount = MockCoins.coin3.sparkline.count
        let graphValuesCount = detailViewModel.graphValues.count
        XCTAssertEqual(sparklineCount, graphValuesCount, "Number of graph values should be equal to the number of sparkline values")
    }
    
}
